
bind = "0.0.0.0:5000"
reload = True
debug = True
workers = 3
errorlog = '/var/log/gunicorn/debug.log'
#loglevel = 'debug'
# proc_name = 'bluestacks_wsgi_gunicorn' # requires installation of setproctitle module

