# ML
BlueStacks Machine learning Stack

Code for bluestacks-learning-1 google compute VM instance hosted @http://146.148.39.215/ 

## Infrastructure
- Python 2.7
- uWSGI based FLASK web framework
- Gunicorn as app server
- nginx as web server
- scikit-learn
- nltk

## Directory Structure
Pull this code inside ```/home/bst/wsgi``` folder so that wsgi folder should have the gunicorn_settings.py, application and main.py and wsgi.py file needed by gunicorn app server at startup.

Directories inside bluestacks folder has been structured according to **Flask Blueprints**.

Each directory can be thought of as a different module with a separate URL prefix (specified in modules __init__.py file)


## Error Logging & Debugging

Web server errors can be accessed here
```
vi /var/log/nginx/error.log
```

Python Errors can be accessed here:
```
vi /var/log/gunicorn/debug.log
```

To clear log file:
```
echo "" > /var/log/gunicorn/debug.log
```

## Server (Service) Start Stop Commands
Command to start Gunicorn server service and server

```sudo systemctl start bluestacks_wsgi```

Command to bind Unicorn server service to start at system boot

```sudo systemctl enable bluestacks_wsgi```

Command to unbind Unicorn server service to not start at system boot

```sudo systemctl disable bluestacks_wsgi```

Command to stop Gunicorn server service and server

```sudo systemctl stop bluestacks_wsgi```

## Server Replication - Configuration
[Original Tutorial Source](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-16-04)

##### Ubuntu Instance settings - Service to Start, Stop & Bind Gunicorn server to system service

```
sudo vi /etc/systemd/system/bluestacks_wsgi.service
```

```
[Unit]
Description=Gunicorn instance to serve bluestacks wsgi
After=network.target

[Service]
User=bst
WorkingDirectory=/home/bst/wsgi
ExecStart=/usr/local/bin/gunicorn --config /home/bst/wsgi/gunicorn_settings.py wsgi:application

[Install]
WantedBy=multi-user.target
```


##### Configuring Nginx to proxy requests

A single Nginx server can listen to multiple ports and multiple IPs (In case our machine has 1 NIC card).

We need to bind the port and IP to the port and IP of our Gunicorn Server (in our case 0.0.0.0:5000)

Create a new server block config for our application.

```
sudo vi /etc/nginx/sites-available/bluestacks_wsgi
```

```
    listen 80;
    server_name server_domain_or_IP;

    location / {
        include proxy_params;
        proxy_pass http://0.0.0.0:5000;
    }
}
```

Mapping our site-available server block to site-enabled via a symlink

```
sudo ln -s /etc/nginx/sites-available/bluestacks_wsgi /etc/nginx/sites-enabled
```

Testing for syntax errors in our configuration and restarting nginx

```
sudo nginx -t
sudo systemctl restart nginx
```





