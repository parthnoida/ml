from bluestacks import application

if __name__ == "__main__":
    # To run Application on Gunicorn usgi application server.
    # Host and port info to be fetched from gunicorn config file
    application.run()
