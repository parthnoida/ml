from bluestacks import application
 
if __name__ == '__main__':
    # To run Application manually of flask Dev server
    application.run(port=5000, debug=True)
