from . import MODULE_STORAGE
import os
import datetime
import numpy as np
import pandas as pd
import traceback
from bigquery import BigQuery
import logging



def log_e(log_str):
    log_file = open(os.path.join(MODULE_STORAGE, 'logs.txt'), 'ab')
    log_file.write("\n" + str(datetime.datetime.utcnow()) + " " + str(log_str))
    log_file.close()


def get_geo_specific_data():
    bq = BigQuery()
    query = """SELECT country, date(timestamp ) as dt, count(distinct guid, 999999) as users, count(*) as installs,
            count(distinct app_pkg ) apps,
            count(distinct IF(source == 'local-apk', guid, NULL), 99999) as local_apk_installs,
            count(distinct IF((source is NULL) OR (source == 'None'), guid, NULL), 99999) as play_store_installs,
            FROM [AppInstallsStats.AppInstallsTwoWeeks]
            where country in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            and is_install = True
            and ((source not in ('spotlight', 'starterbundle')) OR (source is NULL))
            group by country, dt
            order by country, dt desc"""
    raw_bq_data = bq.get_data(query)

    # Converting BigQuery Data to Pandas DataFrame
    bq_data = pd.DataFrame.from_records(raw_bq_data)
    bq_data.apps = bq_data.apps.astype(np.int)
    bq_data.installs = bq_data.installs.astype(np.int)
    bq_data.local_apk_installs = bq_data.local_apk_installs.astype(np.int)
    bq_data.play_store_installs = bq_data.play_store_installs.astype(np.int)
    bq_data.users = bq_data.users.astype(np.int)

    return bq_data


def get_affiliate_app_specific_data():
    query = """SELECT date, country, app_pkg , D0, D1, D2, play_store_install, apk_install,
            case when revenue is NULL then 0 else revenue end as revenue
            from
            (select date, country, B.app_pkg as app_pkg, D0, D1, D2, play_store_install, apk_install, sum(if(date(created_at) == date, payout, NULL)) as revenue
            FROM [AffiliateStats.AffiliateStats] A
            right join each
            (SELECT date, B.country as country, B.app_pkg as app_pkg,
            case when category is null then 'null' else category end as category,
            D0, D1, D2, play_store_install, apk_install
            FROM
            [AppsMetaData.AppsMetaData] A
            right join each
            (SELECT date(B.date) as date, B.country as country, B.app_pkg as app_pkg,
            count(distinct IF(datediff(A.timestamp, B.date)==0, A.guid, NULL),99999) as D0,
            count(distinct IF(datediff(A.timestamp, B.date)==1, A.guid, NULL),99999) as D1,
            count(distinct IF(datediff(A.timestamp, B.date)==2, A.guid, NULL),99999) as D2,
            count(distinct IF((source is NULL or source == 'None'), B.guid, NULL),99999) as play_store_install,
            count(distinct IF(source == 'local-apk', B.guid, NULL),99999) as apk_install
            FROM [AppClickStats.AppClickTwoWeeks] A
            right join each
            (SELECT A.guid as guid, A.app_pkg as app_pkg, B.country as country, B.date as date, source
            FROM [AppClickStats.AppClickTwoWeeks] A
            join each
            (select timestamp(date(timestamp)) as date, A.country as country, A.app_pkg as app_pkg, guid, source
            FROM [AppInstallsStats.AppInstallsTwoWeeks] A
            join each
            (select country_from_ip as country, app_pkg
            FROM [AffiliateStats.AffiliateStats]
            where country_from_ip  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(created_at ) >= date(date_add(current_timestamp() , -15, "DAY") )
            and event_type = 'conversion'
            group by country, app_pkg ) B
            on A.country = B.country and A.app_pkg = B.app_pkg
            where is_install = True) B
            on A.app_pkg = B.app_pkg and A.guid = B.guid and A.timestamp = B.date) B
            on A.guid = B.guid and A.app_pkg = B.app_pkg
            group by date, country, app_pkg
            having date <= date(date_add(current_timestamp() , -3, "DAY"))
            ) B
            on A.pkg_name = B.app_pkg) B
            on A.app_pkg = B.app_pkg and A.country_from_ip = B.country
            where event_type = 'conversion'
            group by date, country , app_pkg, D0, D1, D2, play_store_install, apk_install)
            order by date, country, D0 DESC"""

    raw_bq_data = BigQuery().get_data(query)

    # Converting BigQuery Data to Pandas DataFrame
    bq_data = pd.DataFrame.from_records(raw_bq_data)
    bq_data.D0 = bq_data.D0.astype(np.int)
    bq_data.D1 = bq_data.D1.astype(np.int)
    bq_data.D2 = bq_data.D2.astype(np.int)
    bq_data.play_store_install = bq_data.play_store_install.astype(np.int)
    bq_data.revenue = bq_data.revenue.astype(np.float)
    bq_data.apk_install = bq_data.apk_install.astype(np.int)

    return bq_data


def get_top_app_specific_data(date_str=None):
    if date_str is None or date_str == '':
        date_s = (datetime.timedelta(days=-3) + datetime.datetime.now()).strftime("%Y-%m-%d")
    else:
        date_s = date_str

    """ -- Old Query - To get data for 12 days
            SELECT date, B.country as country, B.app_pkg as app_pkg,
            case when category is null then 'null' else category end as category,
            D0, D1, D2, play_store_install, apk_install
            FROM
            [AppsMetaData.AppsMetaData] A
            right join each
            (SELECT date(B.date) as date, B.country as country, B.app_pkg as app_pkg,
            count(distinct IF(datediff(A.timestamp, B.date)==0, A.guid, NULL),99999) as D0,
            count(distinct IF(datediff(A.timestamp, B.date)==1, A.guid, NULL),99999) as D1,
            count(distinct IF(datediff(A.timestamp, B.date)==2, A.guid, NULL),99999) as D2,
            count(distinct IF((source is NULL or source == 'None'), B.guid, NULL),99999) as play_store_install,
            count(distinct IF(source == 'local-apk', B.guid, NULL),99999) as apk_install
            FROM [AppClickStats.AppClickTwoWeeks] A
            right join each
            (SELECT A.guid as guid, A.app_pkg as app_pkg, B.country as country, B.date as date, source
            FROM [AppClickStats.AppClickTwoWeeks] A
            join each
            (select timestamp(date(timestamp)) as date, A.country as country, A.app_pkg as app_pkg, guid, source
            FROM [AppInstallsStats.AppInstallsTwoWeeks] A
            join each
            (SELECT country, app_pkg from
            (select country_from_ip as country, app_pkg
            FROM [AffiliateStats.AffiliateStats]
            where country_from_ip  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(created_at ) >= date(date_add(current_timestamp() , -15, "DAY") )
            and event_type = 'conversion'
            group by country, app_pkg ),
            (select country, app_pkg
            from
            (select country, app_pkg , installs, ROW_NUMBER() over(partition by country order by installs desc) as rw
            FROM
            (select country, app_pkg, count(distinct guid, 99999) as installs
            FROM [AppInstallsStats.AppInstallsTwoWeeks]
            where country  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(timestamp) >= date(date_add(current_timestamp() , -15, "DAY") )
            and is_install = True
            and ((source not in ('spotlight', 'starterbundle')) OR (source is Null))
            and app_pkg not in ('com.facebook.katana', 'com.amazon.venezia', 'com.instagram.android', 'com.bluestacks.help', 'com.incorporateapps.fakegps.fre', 'com.pop.store', 'com.twitter.android')
            and not REGEXP_MATCH(app_pkg , r'^com.android.*')
            and not REGEXP_MATCH(app_pkg , r'^com.google.*')
            group by country, app_pkg
            having installs >= 10))
            where rw <= 40)
            group by country, app_pkg) B
            on A.country = B.country and A.app_pkg = B.app_pkg
            where is_install = True) B
            on A.app_pkg = B.app_pkg and A.guid = B.guid and A.timestamp = B.date) B
            on A.guid = B.guid and A.app_pkg = B.app_pkg
            group by date, country, app_pkg
            having date <= date(date_add(current_timestamp() , -3, "DAY"))
            ) B
            on A.pkg_name = B.app_pkg
            order by date, country, D0 DESC"""

    """  -- Date Wise Query with Top Apps And Affiliate Apps Combined
            SELECT date, B.country as country, B.app_pkg as app_pkg,
            case when category is null then 'null' else category end as category,
            D0, D1, D2, play_store_install, apk_install
            FROM
            [AppsMetaData.AppsMetaData] A
            right join each
            (SELECT date(B.date) as date, B.country as country, B.app_pkg as app_pkg,
            count(distinct IF(datediff(A.timestamp, B.date)==0, A.guid, NULL),99999) as D0,
            count(distinct IF(datediff(A.timestamp, B.date)==1, A.guid, NULL),99999) as D1,
            count(distinct IF(datediff(A.timestamp, B.date)==2, A.guid, NULL),99999) as D2,
            count(distinct IF((source is NULL or source == 'None'), B.guid, NULL),99999) as play_store_install,
            count(distinct IF(source == 'local-apk', B.guid, NULL),99999) as apk_install
            FROM [AppClickStats.AppClickMonthly]  A
            join each
            (SELECT A.guid as guid, A.app_pkg as app_pkg, B.country as country, B.date as date, source
            FROM [AppClickStats.AppClickMonthly]  A
            join each
            (select timestamp(date(timestamp)) as date, A.country as country, A.app_pkg as app_pkg, guid, source
            FROM [AppInstallsStats.AppInstallsMonthly] A
            join each
            (SELECT country, app_pkg from
            (select country_from_ip as country, app_pkg
            FROM [AffiliateStats.AffiliateStats]
            where country_from_ip  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(created_at ) >= date(date_add(current_timestamp() , -15, "DAY") )
            and event_type = 'conversion'
            group by country, app_pkg ),
            (select country, app_pkg
            from
            (select country, app_pkg , installs, ROW_NUMBER() over(partition by country order by installs desc) as rw
            FROM
            (select country, app_pkg, count(distinct guid, 99999) as installs
            FROM [AppInstallsStats.AppInstallsTwoWeeks]
            where country  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(timestamp) >= date(date_add(current_timestamp() , -15, "DAY") )
            and ((source not in ('spotlight', 'starterbundle')) OR (source is Null))
            and app_pkg not in ('com.facebook.katana', 'com.amazon.venezia', 'com.instagram.android', 'com.bluestacks.help', 'com.incorporateapps.fakegps.fre', 'com.pop.store', 'com.twitter.android')
            and not REGEXP_MATCH(app_pkg , r'^com.android.*')
            and not REGEXP_MATCH(app_pkg , r'^com.google.*')
            group by country, app_pkg
            having installs >= 10))
            where rw <= 40)
            group by country, app_pkg) B
            on A.country = B.country and A.app_pkg = B.app_pkg
            where is_install = True
            and date(timestamp) == '%s') B
            on A.app_pkg = B.app_pkg and A.guid = B.guid and A.timestamp = B.date) B
            on A.guid = B.guid and A.app_pkg = B.app_pkg
            group by date, country, app_pkg
            ) B
            on A.pkg_name = B.app_pkg
            order by date, country, D0 DESC""" % date_s

    # Date Wise query only for top apps
    query = """SELECT date, B.country as country, B.app_pkg as app_pkg,
            case when category is null then 'null' else category end as category,
            D0, D1, D2, play_store_install, apk_install
            FROM
            [AppsMetaData.AppsMetaData] A
            right join each
            (SELECT date(B.date) as date, B.country as country, B.app_pkg as app_pkg,
            count(distinct IF(datediff(A.timestamp, B.date)==0, A.guid, NULL),99999) as D0,
            count(distinct IF(datediff(A.timestamp, B.date)==1, A.guid, NULL),99999) as D1,
            count(distinct IF(datediff(A.timestamp, B.date)==2, A.guid, NULL),99999) as D2,
            count(distinct IF((source is NULL or source == 'None'), B.guid, NULL),99999) as play_store_install,
            count(distinct IF(source == 'local-apk', B.guid, NULL),99999) as apk_install
            FROM [AppClickStats.AppClickTwoWeeks]  A
            join each
            (SELECT A.guid as guid, A.app_pkg as app_pkg, B.country as country, B.date as date, source
            FROM [AppClickStats.AppClickTwoWeeks]  A
            join each
            (select timestamp(date(timestamp)) as date, A.country as country, A.app_pkg as app_pkg, guid, source
            FROM [AppInstallsStats.AppInstallsTwoWeeks] A
            join each
            (select country, app_pkg
            from
            (select country, app_pkg , installs, ROW_NUMBER() over(partition by country order by installs desc) as rw
            FROM
            (select country, app_pkg, count(distinct guid, 99999) as installs
            FROM [AppInstallsStats.AppInstallsTwoWeeks]
            where country  in ('KR', 'US', 'TW', 'RU', 'JP', 'FR', 'IN', 'DE', 'CA', 'GB', 'TR')
            AND date(timestamp) >= date(date_add(current_timestamp() , -15, "DAY") )
            and ((source not in ('spotlight', 'starterbundle')) OR (source is Null))
            and app_pkg not in ('com.facebook.katana', 'com.amazon.venezia', 'com.instagram.android', 'com.bluestacks.help', 'com.incorporateapps.fakegps.fre', 'com.pop.store', 'com.twitter.android')
            and not REGEXP_MATCH(app_pkg , r'^com.android.*')
            and not REGEXP_MATCH(app_pkg , r'^com.google.*')
            group by country, app_pkg
            having installs >= 10))
            where rw <= 40) B
            on A.country = B.country and A.app_pkg = B.app_pkg
            where is_install = True
            and date(timestamp) == '%s') B
            on A.app_pkg = B.app_pkg and A.guid = B.guid and A.timestamp = B.date) B
            on A.guid = B.guid and A.app_pkg = B.app_pkg
            group by date, country, app_pkg
            ) B
            on A.pkg_name = B.app_pkg
            order by date, country, D0 DESC  """ % date_s

    raw_bq_data = BigQuery().get_data(query)

    # Converting BigQuery Data to Pandas DataFrame
    bq_data = pd.DataFrame.from_records(raw_bq_data)
    bq_data.D0 = bq_data.D0.astype(np.int)
    bq_data.D1 = bq_data.D1.astype(np.int)
    bq_data.D2 = bq_data.D2.astype(np.int)
    bq_data.play_store_install = bq_data.play_store_install.astype(np.int)
    bq_data.apk_install = bq_data.apk_install.astype(np.int)

    return bq_data


def bootstrap(number_of_times, sample):
    size = sample.size
    means = np.empty([number_of_times, 1])
    for times in np.arange(number_of_times):
        random_opt = np.random.choice(sample, size=size, replace=True)
        means[times] = np.mean(random_opt)
    means = np.sort(means, axis=0)
    # 95%ile CI
    cil95, ciu95 = np.percentile(means, [2.5, 97.5])
    return cil95, ciu95


def process_geo_data(geo_data):
    result = []
    countries = geo_data.country.unique()
    columns = geo_data.columns
    for c in countries:
        country_h1 = geo_data[geo_data.country == c][:1]   # current
        country_h2 = geo_data[geo_data.country == c][:10]  # old

        intr = {}
        intr['country'] = c
        for col in columns:
            if col not in ['country', 'dt']:
                cil95, ciu95 = bootstrap(10000, country_h2[col])
                c_mean = np.mean(country_h1[col])  # o = old, c = current
                o_mean = np.mean(country_h2[col])
                intr[col + '_c_mean'] = round(c_mean, 0)
                intr[col + '_o_mean'] = round(o_mean, 0)
                if c_mean >= ciu95:
                    intr[col + '_status'] = 1
                elif c_mean <= cil95:
                    intr[col + '_status'] = -1
                else:
                    intr[col + '_status'] = 0

        result.append(intr)
    return result


def process_affiliate_app_data(app_data):
    result = []

    # getting unique country-app pairs to iterate over
    country_apps_list = app_data[['country', 'app_pkg']].drop_duplicates().sort_values(by=['country', 'app_pkg'])

    # calculating retention
    app_data['retention'] = app_data.D1/app_data.D0*100

    # Calculating Rev of each country
    geo_rev = app_data.groupby(by='country', as_index=False).revenue.sum()

    # Fetching data for each country-apps pairs and checking for ALERT country-Apps for last date
    l = country_apps_list.shape[0]
    i = 0
    while i < l:
        country, app_pkg = country_apps_list.iloc[i]
        country_app_data = app_data[(app_data.country == country) & (app_data.app_pkg == app_pkg)]
        country_app_data_prev = country_app_data[:-1]
        country_app_data_cur = country_app_data[-1:]        # Fetch Last date's data

        # If we have more than 3 days worth of previous data AND geo specific app revenue is >= 5% of total geo revenue
        if (country_app_data_prev.shape[0] > 3) and (np.mean(country_app_data.D0) >= 10) and (country_app_data.revenue.sum() >= geo_rev[geo_rev.country == country].iloc[0].revenue * 0.05):
            if country_app_data_cur.iloc[0].retention < (np.mean(country_app_data_prev.retention) * .9):
                temp = {}
                temp['date'] = country_app_data_cur.iloc[0].date
                temp['country'] = country_app_data_cur.iloc[0].country
                temp['app_pkg'] = country_app_data_cur.iloc[0].app_pkg
                temp['D1'] = country_app_data_cur.iloc[0].D1
                temp['retention'] = round(country_app_data_cur.iloc[0].retention, 2)
                temp['deviation'] = round((np.mean(country_app_data_prev.retention) - country_app_data_cur.iloc[0].retention) / np.mean(country_app_data_prev.retention) * 100, 2)

                # Populating data of last 6 days which can be shown in trend - To be implemented in FUTURE
                l2 = min(country_app_data_prev.shape[0], 6)
                i2 = 0
                while i2 < l2:
                    temp['date_' + str(i2)] = country_app_data_prev.iloc[l2 - i2 - 1].date
                    temp['D1_' + str(i2)] = round(country_app_data_prev.iloc[l2 - i2 - 1].D1)
                    temp['retention_' + str(i2)] = round(country_app_data_prev.iloc[l2 - i2 - 1].retention)

                    i2 += 1
                result.append(temp)
        i += 1

    return result


def process_app_data(app_data, params):
    app_data['retention'] = app_data.D1/app_data.D0*100

    l = app_data.shape[0]
    i = 0
    filtered = []

    while i < l:
        temp = []
        rw = app_data.loc[i]
        if rw.category in ['Strategy', 'Adventure', 'Role Playing']:
            if rw.country in params:
                if rw.D0 >= float(params[rw.country]['installs']) and rw.retention < float(params[rw.country]['retention']):
                    temp.append(rw.country)
                    temp.append(rw.app_pkg)
                    temp.append(rw.category)
                    temp.append(round(rw.retention, 2))
                    temp.append(rw.D0)
                    temp.append(rw.D1)
                    temp.append(rw.D2)
                    temp.append(rw.date)
                    filtered.append(temp)
        i += 1

    return filtered

