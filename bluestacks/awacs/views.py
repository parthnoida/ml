from . import awacs, MODULE_STORAGE
import datetime
from flask import render_template
from flask import request, redirect
import json
import os
import pandas as pd
import time
from sendgrid import *
from sendgrid.helpers.mail import *
from utils import *


@awacs.route('/')
def index():

    #  Read data from data file
    data_file_1 = open(os.path.join(MODULE_STORAGE, 'geo_data.txt'), 'rb')
    data_file_2 = open(os.path.join(MODULE_STORAGE, 'app_data.txt'), 'rb')
    data_file_3 = open(os.path.join(MODULE_STORAGE, 'affiliate_data.txt'), 'rb')

    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        geo_data = json.loads(data_file_1.read())
        app_data = json.loads(data_file_2.read())
        aff_data = json.loads(data_file_3.read())
    except:
        app_data = []
        geo_data = []
        aff_data = []

    data_file_1.close()
    data_file_2.close()
    data_file_3.close()

    template_values = {
        'geo_data': geo_data,
        'app_data': app_data,
        'affiliate_data': aff_data
    }

    # send data to frontend
    return render_template('awacs.html', template_values=template_values)


@awacs.route('/sendmail/')
def sendmail():

    #  Read data from data file
    data_file_1 = open(os.path.join(MODULE_STORAGE, 'geo_data.txt'), 'rb')
    data_file_2 = open(os.path.join(MODULE_STORAGE, 'app_data.txt'), 'rb')
    data_file_3 = open(os.path.join(MODULE_STORAGE, 'affiliate_data.txt'), 'rb')

    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        geo_data = json.loads(data_file_1.read())
        app_data = json.loads(data_file_2.read())
        aff_data = json.loads(data_file_3.read())
    except:
        app_data = []
        geo_data = []
        aff_data = []

    data_file_1.close()
    data_file_2.close()
    data_file_3.close()

    template_values = {
        'geo_data': geo_data,
        'app_data': app_data,
        'affiliate_data': aff_data
    }

    # print data to a HTML Template
    html_template = render_template('awacs_email.html', template_values=template_values)

    # Send populated template in email
    sg = SendGridAPIClient(apikey="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")
    mMail = Mail()

    personalization = Personalization()
    personalization.add_to(Email("parth@bluestacks.com"))
    personalization.add_to(Email("lokesh@bluestacks.com"))
    personalization.add_to(Email("suman@bluestacks.com"))
    personalization.add_to(Email("rosen@bluestacks.com"))
    personalization.add_to(Email("ambreesh@bluestacks.com"))

    mMail.from_email = Email("awacs@bluestacks.com")
    mMail.reply_to = Email("parth@bluestacks.com")
    mMail.subject = "AWACS Report - " + str(datetime.datetime.utcnow().strftime("%Y-%m-%d"))
    mMail.add_content(Content("text/html", html_template))
    mMail.add_personalization(personalization)

    response = sg.client.mail.send.post(request_body=mMail.get())

    return str(response.status_code)


@awacs.route('/repopulate/', methods=['GET', 'POST'])
def populate_geos():

    geo_data = get_geo_specific_data()
    geo_data_result = process_geo_data(geo_data)

    data_file = open(os.path.join(MODULE_STORAGE, 'geo_data.txt'), 'wb')
    data_file.write(json.dumps(geo_data_result))
    data_file.close()
    return 'success'


@awacs.route('/repopulate/top_apps/', methods=['GET', 'POST'])
def populate_topapps():

    date_str = request.form.get('date')

    # Fetch Rules/ Params from stored file
    param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'rb')
    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        params = json.loads(param_file.read())
    except:
        params = {}
    param_file.close()

    #  process app specific data
    time.sleep(2)
    app_data = get_top_app_specific_data(date_str)
    app_data_result = process_app_data(app_data, params)

    data_file_2 = open(os.path.join(MODULE_STORAGE, 'app_data.txt'), 'wb')
    data_file_2.write(json.dumps(app_data_result))
    data_file_2.close()
    return 'success'


@awacs.route('/repopulate/affiliate/', methods=['GET', 'POST'])
def populate_affiliate():
    aff_data = get_affiliate_app_specific_data()
    aff_data_result = process_affiliate_app_data(aff_data)

    data_file = open(os.path.join(MODULE_STORAGE, 'affiliate_data.txt'), 'wb')

    data_file.write(json.dumps(aff_data_result))
    data_file.close()
    return 'success'


@awacs.route('/get_params/', methods=['GET'])
def get_params():

    param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'rb')
    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        params = json.loads(param_file.read())
    except:
        params = {}
    param_file.close()

    template_values = {
        "params": params
    }

    return render_template('awacs_params.html', template_values=template_values)


@awacs.route('/create_param/', methods=['POST'])
def create_param():

    country = request.form.get('country')
    installs = request.form.get('installs')
    retention = request.form.get('retention')

    param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'rb')
    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        params = json.loads(param_file.read())
    except:
        params = {}
    param_file.close()

    try:
        if country in params:  # case of edit - country values already exists
            params[country]['installs'] = installs
            params[country]['retention'] = retention

            param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'wb')
            param_file.write(json.dumps(params))
            param_file.close()
            return json.dumps({"status": "success", "message": "Param Updated"})
        else:  # case od add new - Add a new country to params list
            temp = {}
            temp['installs'] = installs
            temp['retention'] = retention
            temp['country'] = country

            params[country] = temp

            param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'wb')
            param_file.write(json.dumps(params))
            param_file.close()
            return json.dumps({"status": "success", "message": "Param Created"})

    except:
        return json.dumps({"status": "error", "message": "Error Creating or Updating User"})


@awacs.route('/delete_param/', methods=['POST'])
def delete_param():

    country = request.form.get('country')

    param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'rb')
    try:
        # Incase the file doesnt exist or has no data - else ast will cause problem at runtime
        params = json.loads(param_file.read())
    except:
        params = {}
    param_file.close()

    try:
        del params[country]
        param_file = open(os.path.join(MODULE_STORAGE, 'params.txt'), 'wb')
        param_file.write(json.dumps(params))
        param_file.close()
        time.sleep(1)
        return redirect("/awacs/get_params")

    except:
        return json.dumps({"status": "error", "message": "Record Not Found"})








