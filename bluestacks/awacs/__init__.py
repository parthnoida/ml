from flask import Blueprint
import os

awacs = Blueprint('awacs', __name__, url_prefix='/awacs', template_folder="templates", static_folder="static")


MODULE_ROOT = os.path.dirname(os.path.abspath(__file__))
MODULE_STATIC = os.path.join(MODULE_ROOT, 'static')
MODULE_STORAGE = os.path.join(MODULE_STATIC, 'storage')

from views import *
