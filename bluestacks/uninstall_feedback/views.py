from . import uninstall_feedback, MODULE_STORAGE
from bigquery import BigQuery
import utils
import datetime
from flask import render_template
import os
import pandas as pd
import pickle
from flask import request
import json


@uninstall_feedback.route('/tagcloud/')
def tag_cloud():
    try:
        pkl_file = open(os.path.join(MODULE_STORAGE, 'misc.pkl'), 'rb')
        bigram_list = pickle.load(pkl_file)
        # bigram_list is a list of dicts in the format 'keyword', 'freq'
        misc = pickle.load(pkl_file)
        pkl_file.close()
        template_values = {
            'bigram_list': bigram_list[:20],
            'last_updated': misc['last_updated']
        }
    except:
        template_values = {'last_updated': 'please repopulate'}

    return render_template('uninstall_feedback.html', template_values=template_values)


@uninstall_feedback.route('/get_feedbacks/', methods=['POST'])
def get_feedbacks():

    ip_str = request.get_json(force=True)['keyword']
    # ip_str = 'google play'
    pkl_file = open(os.path.join(MODULE_STORAGE, 'models.pkl'), 'rb')
    vectorizer = pickle.load(pkl_file)
    k_mean_cluster = pickle.load(pkl_file)
    pkl_file.close()

    pkl_file = open(os.path.join(MODULE_STORAGE, 'data.pkl'), 'rb')
    train = pickle.load(pkl_file)
    feedbacks = pickle.load(pkl_file)
    pkl_file.close()

    result = utils.get_simillar_feedbacks(ip_str, train, feedbacks, vectorizer, k_mean_cluster)
    # result is a list of lists of format - [distance, feedback]:[0, 1]
    return json.dumps(result)


@uninstall_feedback.route('/repopulate/')
def repopulate():
    bq = BigQuery()
    query ="SELECT guid, prod_ver, reason, other_reason, country, timestamp "\
            "FROM [bluestacks-cloud:UninstallFeedback.UninstallFeedback] "\
            "where datediff(current_timestamp() , timestamp) <= 7"\
            "and length(other_reason) > 1 "\
            "order by timestamp desc"
    raw_feedback = bq.get_data(query)

    # Converting BigQuery Data to Pandas DataFrame
    uninstall_feedback = pd.DataFrame.from_records(raw_feedback).sort_values(by='timestamp')
    uninstall_feedback['other_reason'] = uninstall_feedback['other_reason'].apply(utils.prune_other_reason)

    # Nulling and deleting unused variables
    raw_feedback = None
    del raw_feedback

    # Get English Stemmed Count Vectorizer
    vectorizer = utils.get_stemmed_vectorizer(ngram_range=(1, 2), min_df=5)

    # Training feedback data on vectorizer
    train = vectorizer.fit_transform(uninstall_feedback[pd.notnull(uninstall_feedback['other_reason'])]['other_reason'])

    _, bigram = utils.get_anagram_bigram(vectorizer, train)

    # Clustering feedback posts to reduce time complexity while calculating distances
    k_mean_cluster = utils.cluster_feedbacks(train)

    # Pickling bigram
    i = 0
    l = len(bigram)
    bigram_list = []
    while i < l:
        bigram_list.append({'keyword': bigram.iloc[i, 0], 'freq': bigram.iloc[i, 1]})
        i += 1

    misc = {'last_updated': datetime.datetime.utcnow()}

    pkl_file = open(os.path.join(MODULE_STORAGE, 'misc.pkl'), 'wb')
    pickle.dump(bigram_list, pkl_file)
    pickle.dump(misc, pkl_file)
    pkl_file.close()

    # Pickling Data
    pkl_file = open(os.path.join(MODULE_STORAGE, 'data.pkl'), 'wb')
    pickle.dump(train, pkl_file)
    pickle.dump(uninstall_feedback, pkl_file)
    pkl_file.close()

    # Pickling Models
    pkl_file = open(os.path.join(MODULE_STORAGE, 'models.pkl'), 'wb')
    pickle.dump(vectorizer, pkl_file)
    pickle.dump(k_mean_cluster, pkl_file)
    pkl_file.close()

    return str(datetime.datetime.utcnow())
