import json
import logging
import traceback

import httplib2
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client import client as oauth2_client


class BigQuery:

    service = None

    def __init__(self):

        METADATA_SERVER = 'http://metadata/computeMetadata/v1/instance/service-accounts'
        SERVICE_ACCOUNT = 'default'

        http = httplib2.Http()
        token_uri = '%s/%s/token' % (METADATA_SERVER, SERVICE_ACCOUNT)
        resp, content = http.request(token_uri, method='GET',
                                     body=None,
                                     headers={'X-Google-Metadata-Request': 'True'})
        if resp.status == 200:
            d = json.loads(content)
            credentials = oauth2_client.AccessTokenCredentials(d['access_token'], 'my-user-agent/1.0')
            http = credentials.authorize(http)
            self.service = build('bigquery', 'v2', http=http)
        else:
            print "Error"
            raise Exception

    def get_data(self, query_string):
        try:
            query_request = self.service.jobs()
            query_data = {'query': query_string, 'timeoutMs': 700000}

            query_response = query_request.query(projectId='bluestacks-cloud',
                                                 body=query_data).execute()
            if not query_response['jobComplete']:
                raise Exception('Job complete false')

            colNameList = []
            for column_names in query_response['schema']['fields']:
                colNameList.append(str(column_names['name']))

            result_row = []
            if int(query_response['totalRows']) > 0:
                for row in query_response['rows']:
                    obj = {}
                    for index, field in enumerate(row['f']):
                        obj[colNameList[index]] = unicode(field['v']).encode("utf-8")
                    result_row.append(obj)

            return result_row
        except HttpError as err:
            # print 'Error:', str(err.content)
            return {'status', 'error'}

    def async_query(self, query_string):
        #logging.error("async_query BigQueryDataDev File")
        try:
            query_request = self.service.jobs()
            job_data = {
                'configuration': {
                    'query': {
                        'query': query_string
                    }
                }
            }
            #logging.error(job_data)
            query_response = query_request.insert(projectId='bluestacks-cloud',
                                                  body=job_data).execute()
            #logging.error(query_response)

            return query_response['jobReference']['jobId']
        except HttpError as err:
            # print 'Error:', str(err.content)
            return 'error'

    def job_status(self, jobId):
        try:
            query_request = self.service.jobs()

            query_response = query_request.get(projectId='bluestacks-cloud',
                                               jobId=jobId).execute()

            return query_response['status']['state']

        except HttpError as err:
            # print 'Error:', str(err.content)
            return 'error'

    def getQueryResults(self, jobId):
        query_response="error in response"
        try:
            # print 'Query Running: ' + query_string
            query_request = self.service.jobs()
            query_response = query_request.getQueryResults(projectId='bluestacks-cloud',
                                                           jobId=jobId).execute()
            # print "get query results response"
            #print query_response
            if not query_response['jobComplete']:
                raise Exception('Job complete false')

            colNameList = []
            for column_names in query_response['schema']['fields']:
                colNameList.append(str(column_names['name']))

            result_row = []
            if int(query_response['totalRows']) > 0:
                for row in query_response['rows']:
                    obj = {}
                    for index, field in enumerate(row['f']):
                        obj[colNameList[index]] = unicode(field['v']).encode("utf-8")
                    result_row.append(obj)

            return result_row
        except:
            # print 'Error:', str(err.content)
            return str(query_response)+"\n\n"+str(traceback.format_exc())