import numpy as np
import traceback


def bootstrap(number_of_times, pop, size):
    means = np.empty([number_of_times, 1])
    for times in np.arange(number_of_times):
        random_opt = np.random.choice(pop, size=size, replace=True)
        means[times] = np.mean(random_opt)
    return means


def calc_ab_metrices(ctrl_list, b_list):
    ctrl = np.array(ctrl_list)
    ctrl = ctrl.astype(np.int)
    b = np.array(b_list)
    b = b.astype(np.int)
    try:
        tot_pop = np.array(np.append(ctrl, b))
        size = ctrl.size + b.size
        means = bootstrap(50000, tot_pop, size)
        means = np.sort(means, axis=0)
        # 90%ile CI
        cil90, ciu90 = np.percentile(means, [5, 95])
        # 95%ile CI
        cil95, ciu95 = np.percentile(means, [2.5, 97.5])
        b_mean = np.mean(b)
        ctrl_mean = np.mean(ctrl)
        p_val = means[means >= b_mean].shape[0] / float(means.shape[0])
        result = {}
        # calculating if reject H0 and accept H1
        if b_mean > ciu90:
            result['ci90'] = True
        else:
            result['ci90'] = False
        if b_mean > ciu95:
            result['ci95'] = True
        else:
            result['ci95'] = False
        # calculating % increase, if any
        if b_mean > ctrl_mean:
            effect_size = round((b_mean - ctrl_mean)/ctrl_mean * 100, 2)
        else:
            effect_size = -1
        result['a_mean'] = round(ctrl_mean,3)
        result['b_mean'] = round(b_mean,3)
        result['cil90'] = round(cil90,3)
        result['ciu90'] = round(ciu90,3)
        result['cil95'] = round(cil95,3)
        result['ciu95'] = round(ciu95,3)
        result['p_val'] = round(p_val,4)
        result['effect_size'] = round(effect_size,2)
    except Exception:
        result = {}
        result['error_msg'] = str(traceback.format_exc())
    return result







