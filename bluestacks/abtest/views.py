from . import abtest, MODULE_STORAGE
import datetime
from flask import render_template
from flask import request
from flask import make_response
import json
from utils import *

@abtest.route('/')
def index():

    template_values = {}

    return render_template('abtest.html', template_values=template_values)


@abtest.route('/test_results/', methods=['POST'])
def get_results():
    req_json = request.get_json(force=True)
    abc_state = req_json['abc']
    ctrl_raw = req_json['a']
    b_raw = req_json['b']

    ctrl = ctrl_raw.split('\n')
    try:
        ctrl.remove('')
    except:
        pass
    b = b_raw.split('\n')
    try:
        b.remove('')
    except:
        pass

    final_result = {}
    final_result['abc_state'] = abc_state
    final_result['ab'] = calc_ab_metrices(ctrl, b)

    if abc_state == 'abc':
        c_raw = req_json['c']
        c = c_raw.split('\n')
        try:
            c.remove('')
        except:
            pass
        final_result['ac'] = calc_ab_metrices(ctrl, c)
        final_result['bc'] = calc_ab_metrices(b, c)

    return make_response(json.dumps(final_result))


