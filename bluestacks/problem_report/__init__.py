from flask import Blueprint
import os

problem_report = Blueprint('problem_report', __name__, url_prefix='/problem_report', template_folder="templates", static_folder="static")


MODULE_ROOT = os.path.dirname(os.path.abspath(__file__))
MODULE_STATIC = os.path.join(MODULE_ROOT, 'static')
MODULE_STORAGE = os.path.join(MODULE_STATIC, 'storage')

from views import *



