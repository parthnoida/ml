from . import problem_report, MODULE_STORAGE
from bigquery import BigQuery
import utils
import datetime
from flask import render_template
import os
import pandas as pd
import pickle
from flask import request, current_app
import json


@problem_report.route('/tagcloud/')
def tag_cloud():
    current_app.logger.debug('problem_report/tagcloud/')
    try:
        pkl_file = open(os.path.join(MODULE_STORAGE, 'misc.pkl'), 'rb')
        bigram_list = pickle.load(pkl_file)
        # bigram_list is a list of dicts in the format 'keyword', 'freq'
        misc = pickle.load(pkl_file)
        pkl_file.close()
        template_values = {
            'bigram_list': bigram_list[:20],
            'last_updated': misc['last_updated']
        }
    except:
        template_values = {'last_updated': 'please repopulate'}

    return render_template('problem_report.html', template_values=template_values)


@problem_report.route('/get_reports/', methods=['POST'])
def get_reports():

    ip_str = request.get_json(force=True)['keyword']
    pkl_file = open(os.path.join(MODULE_STORAGE, 'models.pkl'), 'rb')
    vectorizer = pickle.load(pkl_file)
    k_mean_cluster = pickle.load(pkl_file)
    pkl_file.close()

    pkl_file = open(os.path.join(MODULE_STORAGE, 'data.pkl'), 'rb')
    train = pickle.load(pkl_file)
    problem_reports = pickle.load(pkl_file)
    pkl_file.close()

    result = utils.get_simillar_reports(ip_str, train, problem_reports, vectorizer, k_mean_cluster)
    # result is a list of lists of format - [distance, problem_reports]:[0, 1]
    return json.dumps(result)


@problem_report.route('/repopulate/')
def repopulate():

    prod_ver_raw = request.args.get('prod_ver')

    prod_ver = ""
    if prod_ver_raw.upper() == 'ALL' or prod_ver_raw == '' or prod_ver_raw is None:
        prod_ver = 'ALL'
    else:
        prod_ver_raw = prod_ver_raw.upper().split(',')
        for v in prod_ver_raw:
            prod_ver += "'"+v+"',"
        prod_ver = prod_ver[:-1]

    query ="SELECT guid, category, log_file , desc, version, timestamp "\
            "FROM [bluestacks-cloud:Logs.DebugLogs] "\
            "where datediff(current_timestamp() , timestamp) <= 7 "\
            "and locale contains 'en' "
    if prod_ver != 'ALL':
        query += "and version in (" + prod_ver + ") "
    query += "order by timestamp desc "

    bq = BigQuery()
    raw_problem_reports = bq.get_data(query)

    # Converting BigQuery Data to Pandas DataFrame
    problem_reports = pd.DataFrame.from_records(raw_problem_reports).sort_values(by='timestamp')
    problem_reports['desc'] = problem_reports['desc'].apply(utils.prune_other_reason)

    # Nulling and deleting unused variables
    raw_problem_reports = None
    del raw_problem_reports

    # Get English Stemmed Count Vectorizer
    vectorizer = utils.get_stemmed_vectorizer(ngram_range=(1, 2), min_df=5)

    # Training reports data on vectorizer
    train = vectorizer.fit_transform(problem_reports[pd.notnull(problem_reports['desc'])]['desc'])

    _, bigram = utils.get_anagram_bigram(vectorizer, train)

    # Clustering reports desc to reduce time complexity while calculating distances
    k_mean_cluster = utils.cluster_reports(train)

    # Pickling bigram
    i = 0
    l = len(bigram)
    bigram_list = []
    while i < l:
        bigram_list.append({'keyword': bigram.iloc[i, 0], 'freq': bigram.iloc[i, 1]})
        i += 1

    misc = {'last_updated': datetime.datetime.utcnow()}

    pkl_file = open(os.path.join(MODULE_STORAGE, 'misc.pkl'), 'wb')
    pickle.dump(bigram_list, pkl_file)
    pickle.dump(misc, pkl_file)
    pkl_file.close()

    # Pickling Data
    pkl_file = open(os.path.join(MODULE_STORAGE, 'data.pkl'), 'wb')
    pickle.dump(train, pkl_file)
    pickle.dump(problem_reports, pkl_file)
    pkl_file.close()

    # Pickling Models
    pkl_file = open(os.path.join(MODULE_STORAGE, 'models.pkl'), 'wb')
    pickle.dump(vectorizer, pkl_file)
    pickle.dump(k_mean_cluster, pkl_file)
    pkl_file.close()

    return str(datetime.datetime.utcnow())
