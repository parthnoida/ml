import scipy as sp
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import MiniBatchKMeans
import nltk.stem
import re

re.MULTILINE
re.DOTALL
re.IGNORECASE


def prune_other_reason(x):
    if x is None:
        return x

    x = str.lower(str(x))
    x = x.replace("doesnt", "doesn't")
    x = x.replace("does not", "doesn't")
    x = x.replace("don't", "doesn't")
    x = x.replace("dont", "doesn't")
    x = x.replace("log in", "login")
    x = x.replace("logged in", "login")
    x = x.replace("sign in", "signin")
    x = x.replace("sign up", "signin")
    x = x.replace("app player", "app_player")
    x = x.replace("stopped", "stop")
    x = x.replace("stops", "stop")
    x = x.replace("Blustack", "bluestacks")
    x = x.replace("blue stack", "bluestacks")
    x = x.replace("bluestack", "bluestacks")
    x = x.replace("blue stacks", "bluestacks")
    x = x.replace("dot.net", "dotnet")
    x = x.replace(".net", "dotnet")
    x = x.replace("dot net", "dotnet")
    return x


def dist_raw(v1, v2):
    # NORMALIZED distance function
    delta = v1/sp.linalg.norm(v1.toarray()) - v2/sp.linalg.norm(v2.toarray())
    return sp.linalg.norm(delta.toarray())


class StemmedCountVectorizer(CountVectorizer):
        def build_analyzer(self):
            english_stemmer = nltk.stem.SnowballStemmer('english')
            analyzer = super(StemmedCountVectorizer, self).build_analyzer()
            return lambda doc: ([re.match(r'[a-zA-Z]', english_stemmer.stem(w)).string if re.match(r'[a-zA-Z]', english_stemmer.stem(w)) is not None else ' ' for w in analyzer(doc)])


def get_stemmed_vectorizer(ngram_range=(1, 2), min_df=5):
    vectorizer_s = StemmedCountVectorizer(ngram_range=ngram_range, min_df=min_df, analyzer="word", stop_words='english')
    return vectorizer_s


def str_word_count(x):
    return len(x.split(' '))


def get_anagram_bigram(vectorizer, train):
    # Getting Top keywords
    # Adding frequency of all words across all feedbacks
    freq = np.ravel(train.sum(axis=0))
    feature_names = np.array(vectorizer.get_feature_names())

    # Actual in-memory rep of trained vectorizer is something like this (f=feature):
    # f1  f2  f3  f4
    # 0   0   1   1
    # 1   0   2   1
    # 0   2   1   0

    # Col Wise Sum =
    # (f1  f2  f3  f4) = (1   2   4   2 )

    # Horizontally stacking feature names and their frequencies in a ndarray
    count_matrix = np.hstack((np.array(feature_names).reshape(feature_names.size, 1), np.array(freq).reshape(freq.size, 1)))
    count_matrix[:, 1] = count_matrix[:, 1].astype(int)

    # converting count matrix ndarray to pandas dataframe
    a = pd.DataFrame(count_matrix)
    # Removing blank values
    a = a[a[0] != ' ']
    # Converting frequencies to integers
    a[1] = a[1].astype(np.int)
    # Adding a new field to dataframe - specifies if the keyword was bigram (2) or anagram (1)
    a[2] = a[0].apply(str_word_count)
    anagram = a[a[2] == 1].sort_values(by=1, ascending=False)
    bigram = a[a[2] == 2].sort_values(by=1, ascending=False)
    return anagram, bigram


def cluster_reports(train):
    # Clustering Vectorized feedbacks
    k_mean_cluster = MiniBatchKMeans(n_clusters=20, init='k-means++', max_iter=100, batch_size=100, verbose=0, compute_labels=True, random_state=None, tol=0.0, max_no_improvement=10, init_size=None, reassignment_ratio=0.1)
    k_mean_cluster.fit(train.toarray())
    # This clustering object also holds the clustered labels_ for each input point
    return k_mean_cluster


def get_simillar_reports(ip_str, train, problem_reports, vectorizer, k_mean_cluster):
    # pruning ip string
    ip_str = prune_other_reason(ip_str)

    # Vactorizing ip string
    ip_vec = vectorizer.transform([ip_str])

    # Clustring ip_vec
    ip_cluster = k_mean_cluster.predict(ip_vec.toarray())
    # getting related posts from same cluster and then calculating the distance.
    result = []
    i = -1
    for label in k_mean_cluster.labels_:
        i += 1
        if ip_cluster[0] == label:
            dist = dist_raw(train[i], ip_vec)
            result.append([i, dist])

    result_df = pd.DataFrame(result)
    result_df = result_df.sort_values(by=[1, 0], ascending=[True, False])
    result_df = result_df[result_df[1] < 1.0]

    #       0	    1
    # 1630	77305	0.765367
    # 1629	77304	0.765367
    # 1592	75004	0.765367
    # 1496	71013	0.765367
    #
    # 0 = index, smaller value means latest feedback
    # 1 = Closeness

    # Getting matched reports
    result = None
    result = []
    i = -1
    for _ in range(result_df.shape[0]):
        i += 1
        ix = result_df.iloc[i][0]
        dx = result_df.iloc[i][1]
        result.append([round(dx, 2), problem_reports.iloc[int(ix)]['desc'], problem_reports.iloc[int(ix)]['version'], problem_reports.iloc[int(ix)]['log_file']])

    return result





