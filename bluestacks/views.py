from bluestacks import main
from . import application

@main.route('/')
@main.route('/<username>')
def index(username=""):
    # return "Welcome to BlueStacks Machine Learning " + str(username)
    application.logger.debug(username)
    return application.make_response("Welcome to BlueStacks Machine Learning " + str(username))