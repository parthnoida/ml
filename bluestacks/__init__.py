from flask import Blueprint, Flask
import os
import logging

from config import Config

application = Flask(__name__, static_folder=os.path.join(os.path.dirname(__file__), "..", "static"))
application.config.from_object(Config)

main = Blueprint('main', __name__)
from views import *

from uninstall_feedback import uninstall_feedback
from abtest import abtest
from awacs import awacs
from problem_report import problem_report
from mod2 import mod2

application.register_blueprint(main)
application.register_blueprint(uninstall_feedback)
application.register_blueprint(abtest)
application.register_blueprint(awacs)
application.register_blueprint(problem_report)
application.register_blueprint(mod2)

gunicorn_error_logger = logging.getLogger('gunicorn.error')
application.logger.handlers.extend(gunicorn_error_logger.handlers)
application.logger.setLevel(logging.DEBUG)


